#!/usr/bin/env python3

"""A module for joining and splitting CSV files."""

from pathlib import Path

import pandas as pd
import pymsgbox


def list_csv_files(Directory: (Path, str) = Path(".").resolve()) -> list:
    """Scans the current directory and builds a list of '.csv' files.
    Takes an optional argument, 'Directory', to specify the directory used.
    Returns a list of these files."""
    csv_files = list()
    for file in Directory.iterdir():
        if file.suffix.lower() == ".csv":
            csv_files.append(file)
    # Exit program if no CSV files are found
    if len(csv_files) == 0:
        pymsgbox.alert("No CSV files found in:\n\n"
                       f"{Directory}")
        exit(-1)  # No CSV files to join
    return csv_files


def csv_to_dataframe(Csv_Files: list) -> list:
    """Opens 'Csv_Files' as a pd.DataFrame, and returns them as a list.
    """
    return [pd.read_csv(file, header=None) for file in Csv_Files]


def split_dataframe(DataFrame: pd.DataFrame, CHUNKSIZE: int = 500) -> list:
    """Takes a single 'pandas.DataFrame' and splits it into max-sized chunks.
    :type DataFrame: pd.DataFrame
    :type CHUNKSIZE: int
    :returns list of DataFrames."""
    dataframes = list()
    CHUNK_COUNT = (len(DataFrame) // CHUNKSIZE) + 1  # Floor division + 1
    for i in range(CHUNK_COUNT):
        dataframes.append(DataFrame[(i * CHUNKSIZE):((i + 1) * CHUNKSIZE)])
    return dataframes


def joiner(Csv_File: (Path, str) = Path("Merged.csv").resolve()) -> None:
    """Scans the current working directory for CSV files and
    concatenates them using 'pandas'. The file is saved as 'Csv_File'.
    By default, 'Csv_File' is saved as 'Merged.csv' in the current
    working directory.
    :type Csv_File: (Path, str)"""
    CSV_DIRECTORY = Path(".").resolve() / "CSV_Files_Here"
    csv_files = list_csv_files(CSV_DIRECTORY)
    # Create Merged DataFrame
    merged_dataframe = pd.concat(csv_to_dataframe(csv_files), ignore_index=True)
    # Save dataframe
    merged_dataframe.to_csv(Csv_File, header=None, index=False,
                            encoding="utf-8")
    return None


def splitter(Csv_File: (Path, str), ROW_COUNT: int = 400) -> None:
    """Opens 'Csv_File' and splits it into multiple files containing
    'ROW_COUNT' rows.
    The new files are named using 'Csv_File's name, appending '.n' to
    each filename, where 'n' is the number of the split CSV file.
    :type Csv_File: (Path, str)
    :type ROW_COUNT: int"""
    # Cast 'Csv_File' to a 'Path'
    if not isinstance(Csv_File, Path):
        Csv_File = Path(Csv_File).resolve()
    DataFrame = pd.read_csv(Csv_File, header=None)
    dfs = split_dataframe(DataFrame, CHUNKSIZE=400)
    # Save each dataframe to a separate CSV file
    for n, df in enumerate(dfs, 1):
        df.to_csv(f"{Csv_File.stem}.{n}.csv", header=None, index=False)
    return None
