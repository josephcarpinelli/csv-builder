#!/usr/bin/env python3

"""Generates a combined CSV file that is split into separate files
with a maximum number of rows per file."""


from datetime import date

import pymsgbox

from modules import csv_builder


def main() -> int:
    # Get filename from user
    merged_filename = pymsgbox.prompt("Please enter the file to split: ")
    if not merged_filename.lower().endswith(".csv"):
        merged_filename = "".join([merged_filename, ".csv"])

    # File saving
    try:
        csv_builder.splitter(merged_filename, 400)
    except IOError:
        pymsgbox.alert("Error saving file, merged CSV file not split!")
        return -1  # IOError, files not saved

    return 0


if __name__ == "__main__":
    main()
